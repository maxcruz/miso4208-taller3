import {browser, by, element, ElementFinder} from 'protractor';

export class TourOfHeroesPage {

  navigateTo() {
    return browser.get('/');
  }

  getTop4Heroes() {
    return element.all(by.css('.module.hero')).all(by.tagName('h4')).getText();
  }

  navigateToHeroes() {
    element(by.linkText('Heroes')).click();
  }

  navigateToHeroDetail(name: string){
    element(by.cssContainingText('span', name)).click();
    element(by.cssContainingText('button', 'View Details')).click();
  }

  navigateToHeroFromTop(name: string){
    element(by.cssContainingText('h4', name)).click()
  }

  getHeroId(name: string) {
    return element.all(by.cssContainingText('span', name))
      .all(by.xpath('preceding-sibling::span')).getText();
  }
 
  getHeroDetailTitle() {
    return element.all(by.tagName('h2')).getText();
  }

  getAllHeroes() {
    return element(by.tagName('my-heroes')).all(by.tagName('li'));
  }

  enterNewHeroInInput(newHero: string) {
    element(by.tagName('input')).sendKeys(newHero);
    element(by.buttonText('Add')).click();
  }

  deleteHero(name: string) {
    element.all(by.cssContainingText('span', name))
      .all(by.xpath('following-sibling::button')).click();
  }

  editHero(newName: string) {
    element(by.tagName('input')).clear().then(function() {
      element(by.tagName('input')).sendKeys(newName);
    });
    element(by.cssContainingText('button', 'Save')).click();
  }

  searchHeroName(name: string) {
    element(by.id('search-box')).sendKeys(name);
    return element(by.css('.search-result')).getText();
  }

  navigateToHeroFromSearch(name: string) {
    element(by.id('search-box')).sendKeys(name).then(function() {  
      element(by.css('.search-result')).click(); 
    });
  }

 }
