var assert = require('assert');
describe('los estudiantes login', function() {

    it('Fails at login', function () {
        browser.url('https://losestudiantes.co');
        browser.click('button=Cerrar');
	browser.pause(1000);
	browser.waitForVisible('button=Ingresar', 5000);
	console.log('HERE')
        browser.click('button=Ingresar');
	browser.waitForVisible('.cajaLogIn', 5000);
        var cajaLogIn = browser.element('.cajaLogIn');
	cajaLogIn.setValue('input[name="correo"]', 'wrongemail@example.com')
        cajaLogIn.setValue('input[name="password"]', '12345')        
        cajaLogIn.element('button=Ingresar').click()
        browser.waitForVisible('.aviso.alert.alert-danger', 5000);
        var alertText = browser.element('.aviso.alert.alert-danger').getText();
        expect(alertText).toBe('Upss! El correo y la contraseña que ingresaste no figuran en la base de datos. Intenta de nuevo por favor.');
    });

    it('Try create registered account', function() {
        var cajaSignUp = browser.element('.cajaSignUp')
        cajaSignUp.setValue('input[name="nombre"]', 'Max')
        cajaSignUp.setValue('input[name="apellido"]', 'Cruz')
	cajaSignUp.setValue('input[name="correo"]', 'mr.cruz@uniandes.edu.co')
	cajaSignUp.selectByVisibleText('select[name="idUniversidad"]', 'Universidad de los Andes')
	cajaSignUp.selectByVisibleText('select[name="idDepartamento"]', 'Ingeniería de Sistemas')
        cajaSignUp.setValue('input[name="password"]', '12345678')
	cajaSignUp.element('input[name="acepta"]').click()
        cajaSignUp.element('button=Registrarse').click()
        browser.waitForVisible('.sweet-alert', 5000)
	var cajaAlert = browser.element('div.sweet-alert').element('div.text-muted')
	var error = cajaAlert.getText('div')
	expect(error).toBe('Error: Ya existe un usuario registrado con el correo \'mr.cruz@uniandes.edu.co\'')
	browser.element('button=Ok').click()
    });

    it('Right login', function() {
        var cajaLogIn = browser.element('.cajaLogIn');
	cajaLogIn.setValue('input[name="correo"]', '');
	cajaLogIn.setValue('input[name="correo"]', 'mr.cruz@uniandes.edu.co');
	cajaLogIn.setValue('input[name="password"]', '');
        cajaLogIn.setValue('input[name="password"]', 'l0535miso');
	cajaLogIn.element('.logInButton').click();;
	browser.pause(1000);
	browser.waitForVisible('#cuenta', 5000);
        browser.element('#cuenta').click()
        browser.waitForVisible('.dropdown-menu', 5000)
	var menu = browser.element('.dropdown-menu').element('li').getText('a')
        expect(menu).toBe('Salir');
    });

    it('Search teachers', function() {
        browser.element('.Select-placeholder').click()
	browser.element('.Select-input').setValue('input', 'Mario')
	browser.waitForVisible('.Select-menu-outer', 5000);
    })

});
