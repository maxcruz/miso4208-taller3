'use strict';

const Audit = require('lighthouse').Audit;

const MAX_REST_TIME = 3000;

class LoadAudit extends Audit {
    static get meta() {
        return {
            category: 'MyPerformance',
            name: 'endpoint-audit',
            description: 'Audit call to the rest api',
            failureDescription: 'Api call slow to initialize',
            helpText: 'Used to measure the time to request to the api call',
            requiredArtifacts: ['TimeToRest']
        };
    }

    static audit(artifacts) {
        const loadedTime = artifacts.TimeToRest;

        const belowThreshold = loadedTime <= MAX_REST_TIME;

        return {
            rawValue: loadedTime,
            score: belowThreshold
        };
    }
}

module.exports = LoadAudit;
